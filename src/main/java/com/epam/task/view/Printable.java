package com.epam.task.view;

public interface Printable {

    void print();
}
