package com.epam.task.view;

import com.epam.task.controller.Controller;
import com.epam.task.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MyView {

    private Locale locale;
    private ResourceBundle bundle;
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }


    public MyView() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        controller = new ControllerImpl();
        setMenu();
        this.methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
    }

    private void pressButton1() {
        System.out.println("Concatenates list:");
        System.out.println(controller.getConcatenatesList());
    }

    private void pressButton2() {
        System.out.println("English menu:");
        getEnglishMenu();
    }

    private void pressButton3() {
        System.out.println("Ukraine menu:");
        getUkraineMenu();
    }

    private void pressButton4() {
        System.out.println("Is word begin with capital letter and ends on period?");
        System.out.println(controller.checkEnteredWord());
    }

    private void pressButton5() {
        System.out.println("Divided sentence:");
        System.out.println(controller.splitEnteredSentence());
    }

    private void pressButton6() {
        System.out.println("Replaced all the vowels with underscores:");
        System.out.println(controller.replaceVowel());
    }

    //------------------------------------------------------------------------------------

    public void getEnglishMenu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
    }

    public void getUkraineMenu() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
    }

    //--------------------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
