package com.epam.task.controller;


public interface Controller {

    String getConcatenatesList();

    boolean checkEnteredWord();
    
    String splitEnteredSentence();
    
    String replaceVowel();

}
