package com.epam.task.controller;

import com.epam.task.model.Logic;
import com.epam.task.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new Logic();
    }

    @Override
    public String getConcatenatesList() {
        return model.getConcatenatesList();
    }

    @Override
    public boolean checkEnteredWord() {
        return model.checkEnteredWord();
    }

    @Override
    public String splitEnteredSentence() {
        return model.splitEnteredSentence();
    }

    @Override
    public String replaceVowel() {
        return model.replaceVowel();
    }
}