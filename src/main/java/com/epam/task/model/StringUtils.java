package com.epam.task.model;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
    private static Scanner scanner = new Scanner(System.in);
    String concatenatedList = "";

    public String getConcatenatesList() {
        String in;
        do {
            System.out.println("Please, enter word...");
            in = scanner.nextLine();
            concatenatedList += in + " ";
        } while (!in.equals(""));
        return concatenatedList;
    }

    public boolean checkEnteredWord() {
        String sentence;
        System.out.println("Please, enter word...");
        sentence = scanner.nextLine();
        Pattern p = Pattern.compile("\\b([A-Z][a-z]*)\\b(.$)");
        Matcher m = p.matcher(sentence);
        return m.matches();
    }

    public String splitEnteredSentence() {
        String input;
        System.out.println("Please, enter sentence...");
        input = scanner.nextLine();
        String[] splited = (input.split("\\bthe|you\\b"));
        StringBuilder stringBuilder = new StringBuilder();
        for (String s:splited) {
            stringBuilder.append(s);
        }
        return stringBuilder.toString().trim();
    }

    public String replaceVowel() {
        String text;
        System.out.println("Please, enter word...");
        text = scanner.nextLine();
        return text.replaceAll("[aeoiu]","_");
    }
}