package com.epam.task.model;

import java.util.List;

public interface Model {

    public String getConcatenatesList();

    public boolean checkEnteredWord();

    public String splitEnteredSentence();

    public String replaceVowel();
}
